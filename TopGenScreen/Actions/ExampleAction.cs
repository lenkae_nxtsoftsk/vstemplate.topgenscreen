﻿using ACE.Agresso.Utils.Action;
using ACE.Agresso.Utils.Connector;
using Agresso.Interface.TopGenExtension;
using System.Data;

namespace TopGenScreen.Actions
{
    public class ExampleAction : IAction
    {
        private IConnector connector;
        private IForm form;
        private DataSet data;
        private IAction saveAction;

        public ExampleAction(IForm form, IConnector connector, DataSet data, IAction saveAction)
        {
            this.form = form;
            this.connector = connector;
            this.data = data;
            this.saveAction = saveAction;
        }

        public void Execute()
        {
            // do something wonderful

            saveAction.Execute();
        }
    }
}
