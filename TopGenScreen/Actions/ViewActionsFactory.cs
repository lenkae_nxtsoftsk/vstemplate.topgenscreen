﻿using ACE.Agresso.Utils.Action;
using ACE.Agresso.Utils.Connector;
using Agresso.Interface.TopGenExtension;
using System.Data;

namespace TopGenScreen.Actions
{
    public class ViewActionsFactory : IViewActions
	{
		public IAction GetSaveAction(DataSet data, IConnector connector)
		{
			return new SaveAction(data, connector);
		}

        public IAction GetExampleAction(IForm form, IConnector connector, DataSet data, IAction saveAction)
        {
            return new ExampleAction(form, connector, data, saveAction);
        }
    }
}
