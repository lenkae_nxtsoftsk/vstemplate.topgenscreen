﻿using ACE.Agresso.Utils.Action;
using ACE.Agresso.Utils.Connector;
using Agresso.Interface.CommonExtension.Data;
using System;
using System.Data;

namespace TopGenScreen.Actions
{
    public class SaveAction : IAction
    {
        DataSet data;
        IConnector connector;

        public SaveAction(DataSet data, IConnector connector)
        {
            this.data = data;
            this.connector = connector;
        }

        public void Execute()
        {
            if (data.HasChanges())
            {
                try
                {
                    DataSetSaver saver = new DataSetSaver();
                    //connector.SaveData(data, new TableDefinition(), true, true);
                }
                catch (Exception ex)
                {
                    connector.DisplayErrorMessage(ex.Message);
                }
            }
        }
    }
}
