﻿using ACE.Agresso.Utils.Action;
using ACE.Agresso.Utils.Connector;
using Agresso.Interface.TopGenExtension;
using System.Data;

namespace TopGenScreen.Actions
{
    public interface IViewActions
    {
        IAction GetSaveAction(DataSet data, IConnector connector);
        IAction GetExampleAction(IForm form, IConnector connector, DataSet data, IAction saveAction);
    }
}
