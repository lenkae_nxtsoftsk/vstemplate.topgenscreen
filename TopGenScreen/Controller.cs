﻿using Agresso.Interface.CommonExtension.Data;
using Agresso.Interface.TopGenExtension.UseCase;
using System.Data;
using ACE.Agresso.Utils.Connector;

namespace TopGenScreen
{
    [TopGenUseCase("customFunctionName", "customDescription")]
	public class Controller : IProjectUseCase
	{
		IConnector connector = null;

		public void Initialize(IUseCaseEvents events)
		{
			InitializeConnector();
			InitializeEvents(events);
		}
	
		public void InitializeConnector(IConnector conn = null)
		{
			if (this.connector == null)
			{
				this.connector = conn ?? new DefaultConnector();
			}
		}

		private void InitializeEvents(IUseCaseEvents events)
		{
			events.OnCreateStructure += OnCreateStructure;
			events.OnAddRow += OnAddRow;

            //events.OnGetValueList += OnGetValueList;
        }
        
		private void OnAddRow(object sender, AddDataRowEventArgs e)
		{
            // This handler has to be here, even if empty, otherwise adding new rows doesn't work
        }

		private void OnCreateStructure(object sender, CreateStructureEventArgs e)
		{
			e.Data = new DataSet();

            // define which tables will be presented in dataset
            DefineDataModel(e.Data);
            
			LoadUiData(e.Data);
            LoadData(e.Data);
		}

        private void LoadUiData(DataSet data)
        {
            //do something wonderful
        }

        private void LoadData(DataSet data)
        {
            //do something wonderful
        }

        private void DefineDataModel(DataSet data)
		{
			DataSetBuilder builder = new DataSetBuilder();
			//builder.AppendDataSetStructure(data, new TableDefinition());
        }
    }
}
