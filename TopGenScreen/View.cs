﻿using ACE.Agresso.Utils.Action;
using ACE.Agresso.Utils.Connector;
using ACE.Agresso.Utils.Data;
using ACE.Agresso.Utils.FormAccess;
using ACT.Common.Data;
using Agresso.Interface.TopGenExtension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using TopGenScreen.Actions;

namespace TopGenScreen
{
    [TopGen("customFunctionName", "*", "*", "customDescription")]
    public class View : IProjectTopGen
    {
        private IConnector connector;
        private IFormAccess uiFormAccess;
        private IViewActions viewActions;

        private void InitializeConnector()
        {
        }

        public IViewActions Actions
        {
            get
            {
                if(viewActions == null)
                {
                    viewActions = new ViewActionsFactory();
                }

                return viewActions;
            }

            set
            {
                viewActions = value;
            }
        }

        public IConnector Connector
        {
            get
            {
                if(connector == null)
                {
                    connector = new DefaultConnector();
                }

                return connector;
            }
            set
            {
                connector = value;
            }
        }

        public void Initialize(IForm form)
		{
            uiFormAccess = new UiFormAccess(form);
			InitializeConnector();
			InitializeConnectorDependencies();

            form.OnCallingAction += OnCallingAction;
            //form.OnValidatedField += OnValidatedField;
            //form.OnSectionTreeAction += OnSectionTreeAction;
            //form.OnSearchList += OnSearchList;
            //form.OnDeletingRows += OnDeletingRows;
            //form.OnAddedRow += OnAddedRow;
            //form.OnScreenRefreshed += Form_OnScreenRefreshed;
        }

        private void InitializeConnectorDependencies()
		{
			Table.Connector = Connector;
		}

		private void OnCallingAction(object sender, ActionEventArgs e)
		{
            IAction saveAction = Actions.GetSaveAction(uiFormAccess.Form.Data, Connector);

            switch (e.Action)
			{
                case Constants.ACTION_NAME_EXAMPLE:
                    Actions.GetExampleAction(uiFormAccess.Form, Connector, uiFormAccess.Form.Data, saveAction).Execute();
                    break;
            }
        }

        private DataRow GetSelectedRow(ISection section)
        {
           return section.GetSelectedDataRow();
        }

        private void ResetSectionSelection(ISection targetFormSection)
        {
            //EXAMPLE: uiFormAccess.Form.Sections[PAGE_TAB_REQUISITION].Sections[PAGE_SECTION_HEADER].Sections[PAGE_LIST_HEADER].ClearSelectedRow();
            targetFormSection.ClearSelectedRow();
        }

        private void OnSearchList(object sender, ValueListEventArgs e)
        {
            if (null == e.List)
            {
                return;
            }

            if (e.HasSearchFilter)
            {
                e.List = GetFilteredList(e.List, e.SearchFilter, e.TableName);
            }
        }

        private IValueDescriptionList GetFilteredList(IValueDescriptionList list, string filteringString, string tableName)
        {
            if (null == list || String.IsNullOrEmpty(filteringString))
            {
                return list;
            }

            var results = FilterList(list, filteringString);

            return GetListFromSearchResult(results);
        }

        private IValueDescriptionList GetListFromSearchResult(IEnumerable<IValueDescriptionPair> results)
        {
            IValueDescriptionList filteredList = ValueDescriptionListFactory.Create<String>();
            foreach (IValueDescriptionPair keyValue in results)
            {
                filteredList.Add(keyValue.Value, keyValue.Description);
            }
            return filteredList;
        }

        private IEnumerable<IValueDescriptionPair> FilterList(IValueDescriptionList list, String filteringString)
        {
            int c = new Func<int>(() => { return 5 * 4; })();

            var entries = filteringString.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries);
            var regexPattern = string.Join(".*", entries.Select(x => Regex.Escape(x)));
            var regex = new Regex(regexPattern, RegexOptions.IgnoreCase);
            var results = from item in list
                          where new Func<bool>(() =>
                          {
                              var matchDescr = regex.Match(item.Description);
                              var matchValue = regex.Match(item.ValueText);
                              return matchDescr.Success || matchValue.Success;
                          })()
                          select item;
            return results;
        }
    }
}
