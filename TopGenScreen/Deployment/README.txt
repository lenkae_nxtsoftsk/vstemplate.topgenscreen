1. screen_example.atg - screen layout editable via UNIT4 Customization Tools Studio

2. deploy.ps - PowerShell script, needs to be run via PowerShell console (Run As Administrator mode)

!IMPORTANT!
Check predefined parameters in deployment scripts like DB access and paths!
