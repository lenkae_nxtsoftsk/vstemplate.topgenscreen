﻿INSERT INTO agldimension (att_name, attribute_id, client, data_length, data_type, description, dim_grp, dim_position, dim_v1_txt, last_update, maintenance, period_type, rel_attr_id, related_attr, status, user_id,bflag) VALUES (N'ARTGRP-EBENE1', N'EB1','EN',12, N'A', N'Artikelgruppe Ebene 1', 0, N'X', N'', getdate(), N'M', 0, N'', N'', N'N', 'SYSEN',15)

DECLARE @articleId int;
SET @articleId = (SELECT max(article_id) FROM algarticle);
SET @articleId = @articleId + 1;
INSERT INTO agldimvalue (description,rel_value,value_1,period_to,status,client,last_update,user_id,attribute_id,dim_value,period_from) VALUES (N'Basecap (GVD)',N'',0.00000000,209999,N'N','EN',getdate(),'SYSEN',N'D0',N'11001',0 );
INSERT INTO algarticle (amount_set,art_descr,article,article_id,bflag,client,last_update,max_level,status,tax_code_po,unique_flag,unit_code,user_id,public_product) VALUES (0,N'Basecap (GVD)',N'11001',@articleId,3,'EN',getdate(),99999999.99000000,'N',35,1,'ST','SYSEN',1);
INSERT INTO algrelvalue (article_id,client,last_update,rel_value,related_attr,user_id) VALUES (@articleId,'EN',getdate(),'01/10/00/00','EB4','SYSEN');
INSERT INTO algunit (article_id, client, description, last_update, main_unit, unit_code, user_id) VALUES (@articleId, 'EN', 'Pieces', getdate(),'ST','ST','SYSEN');
