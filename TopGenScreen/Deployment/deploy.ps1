# (!)CHECK parameter values
param(
    [string]$Client = "EN", # used in sql scripts
    [string]$UserId = "SYSEN", # used in sql scripts
    [string]$Lang = "EN", # used in sql scripts
	[string]$WSUser = "sysenlong", # used in sql scripts
	[string]$WSPwd = "agresso", # used in sql scripts
    [string]$Datasource = "agressoM7", # used to upload .dll and .atg
    [string]$BackupPath = "C:\SQLDATA", # used in backuping up sql database
	[string]$ServerInstance = "localhost", # used when invoking .sql files
	[string]$DbName = "agr700WithoutXP", # used when invoking .sql files
	[string]$Username = "agressoM7", # used when invoking .sql files
	[string]$Password = "agressoM7", # used when invoking .sql files
	[string]$VoucherType = "IB", # used when invoking .sql files
	[string]$DummyArticleId = "102" # used when invoking .sql files
)

Write-Host "Datasource: " $Datasource
Write-Host "ServerInstance: " $ServerInstance
Write-Host "DbName: " $DbName
Write-Host "Username: " $Username
Write-Host "Password: " $Password

Write-host "The deployment has started..." -ForegroundColor Magenta

if([string]::IsNullOrEmpty($env:AGRESSO_HOME))
{
    $env:AGRESSO_HOME = 'C:\Program Files (x86)\UNIT4 Business World M6\Bin'
}
if([string]::IsNullOrEmpty($env:AGRESSO_WS_HOME))
{
    $env:AGRESSO_WS_HOME = 'C:\Program Files\UNIT4 Business World M6\Web Services\Business World Web Services\Bin'
}
if([string]::IsNullOrEmpty($env:AGRESSO_START_URL))
{
    $env:AGRESSO_START_URL = 'http://localhost/BusinessWorld/Login/Login.aspx'
}
if([string]::IsNullOrWhiteSpace($env:ACT))
{
    $env:ACT = "$env:AGRESSO_HOME\actsetup.exe"
}
if([string]::IsNullOrWhiteSpace($env:TG))
{
    $env:TG = "$env:AGRESSO_HOME\tgsetup.exe"
}
Write-host "Environment variables:" -ForegroundColor Magenta
Write-host "  AGRESSO_HOME: $env:AGRESSO_HOME" -ForegroundColor Magenta
Write-host "  AGRESSO_WS_HOME: $env:AGRESSO_WS_HOME" -ForegroundColor Magenta
Write-host "  ACT: $env:ACT" -ForegroundColor Magenta
Write-host "  TG: $env:TG" -ForegroundColor Magenta

if([string]::IsNullOrWhiteSpace($env:AGRESSO_HOME) -Or [string]::IsNullOrWhiteSpace($env:AGRESSO_WS_HOME))
{
    Write-host "Environment variables AGRESSO_HOME or AGRESSO_WS_HOME are not set!" -ForegroundColor Red
	Exit
}

# ------- REGISTER PROJECT DLLs SECTION (created post-build event above particular project to copy dlls to AGRESSO_HOME and Deployment directory)

./register_dlls.ps1 -Database $DbName -Server $ServerInstance -DBUser $Username -DBPassword $Password

# ------- REGISTER ATG FILES SECTION
# (!)REPLACE values <screen_example> and <root namespace> (for example <screen_example>.atg will become myscreen.atg)

$fileNameScreen = (Get-ChildItem <screen_example>.atg).fullname
$fileNameScreenShort = [System.IO.Path]::GetFileName($fileNameScreen)
Write-host "Registering screen: $fileNameScreenShort"   -ForegroundColor Magenta
Start-Process "$env:TG" "/l /a:<root namespace> /D:$Datasource $fileNameScreenShort" -NoNewWindow -Wait

# ------- BACKUP SCREEN SECTION
# Backup-SqlDatabase -ServerInstance $ServerInstance -Database $Database -BackupFile "$BackupPath\$Database.bak"

# ------- CUSTOM SQL SCRIPTS SECTION

# $vars = "Client = '$Client'", 
#        "UserId = '$UserId'",
#        "Lang = '$Lang'",
#		"WSUser = '$WSUser'",
#		"WSPwd = '$WSPwd'",
#		"VoucherType = '$VoucherType'",
#		"DummyArticleId = '$DummyArticleId'"

# $custom_attributes = [System.IO.Path]::GetFileName("example_data.sql")
# Write-host "Creating custom attributes from: $custom_attributes" -ForegroundColor Magenta
# Invoke-Sqlcmd -ServerInstance $ServerInstance -Database $DbName -InputFile $custom_attributes -Variable $vars -Username $Username -Password $Password

Write-host "...finished" -ForegroundColor Magenta