﻿using ACE.Agresso.Utils.Connector;
using Agresso.Interface.TopGenExtension;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Data;

namespace TopGenScreen.UT
{
    [TestFixture]
    public abstract class TestBase
    {
        protected Mock<IConnector> Connector { get; set; }
        protected DataSet data = null;
        protected Mock<IForm> form = null;

        [SetUp]
        public void SetUp()
        {
            Connector = new Mock<IConnector>();
            data = InitializeEmptyDataSet();
            form = new Mock<IForm>();
            form.Setup(foo => foo.Data).Returns(data);
        }

        protected void SetBasicConnectorReturns(Mock<IConnector> connector, string client, string userId, string language, string sysSetupCode)
        {
            connector.Setup(foo => foo.GetClient()).Returns(client);
            connector.Setup(foo => foo.GetUserId()).Returns(userId);
            connector.Setup(foo => foo.GetLanguage()).Returns(language);
            connector.Setup(foo => foo.GetSysSetupCode()).Returns(sysSetupCode);
        }

        protected abstract DataSet InitializeEmptyDataSet();

        /// <summary>
        /// Defines titles to be returned by calling IConnector's GetTitle method.
        /// </summary>
        /// <param name="titles">Loaded titles</param>
        protected void SetConnectorTitleResponse(IDictionary<int, string> titles)
        {
            Connector.Setup(foo => foo.GetTitle(It.IsAny<int>(), It.IsAny<string>()))
                .Returns<int, string>((titleId, defaultValue) =>
                   {
                       return GetValueFromDictionary(titles, titleId, defaultValue);
                   }
                );

            Connector.Setup(foo => foo.GetTitle(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<object[]>()))
                .Returns<int, string, object[]>((titleId, defaultValue, parameters) =>
                {
                    return string.Format(GetValueFromDictionary(titles, titleId, defaultValue), parameters);
                }
                );
        }

        /// <summary>
        /// Defines common parameters to be returned by calling IConnector's GetCommonParameter method.
        /// </summary>
        /// <param name="commonParameteres">Loaded common parameters</param>
        protected void SetCommonParametersResponse(IDictionary<string, string> commonParameteres)
        {
            Connector.Setup(foo => foo.GetCommonParameter(It.IsAny<string>(), It.IsAny<string>()))
                .Returns<string, string>((parameterName, defaultValue) 
                => GetValueFromDictionary(commonParameteres, parameterName, defaultValue) );
        }

        #region Helpers

        private string GetValueFromDictionary(IDictionary<int, string> values, int key, string defaultValue)
        {
            if (values.ContainsKey(key))
            {
                return values[key];
            }
            return defaultValue;
        }

        private string GetValueFromDictionary(IDictionary<string, string> values, string key, string defaultValue)
        {
            if (values.ContainsKey(key))
            {
                return values[key];
            }
            return defaultValue;
        }

        #endregion
    }
}
