﻿using ACE.Agresso.Utils.Action;
using ACE.Agresso.Utils.FormAccess;
using Agresso.Interface.CommonExtension.Data;
using Agresso.Interface.TopGenExtension;
using Moq;
using NUnit.Framework;
using System.Data;
using TopGenScreen.Actions;

namespace TopGenScreen.UT
{
    public class ViewTestBase : TestBase
    {
        protected Mock<IFormViewState> viewState = null;
        protected Mock<ITopGenCommandFactory> commandFactory = null;
        protected Mock<IDecorator> decorator = null;
        protected Mock<IDialogHandler> dialogs = null;
        protected Mock<IFormAccess> formAccess = null;
        protected Mock<IViewActions> viewActions = null;
        protected Mock<IAction> saveAction = null;
        protected View view = null;
        
        [SetUp]
        public new void SetUp()
        {
            viewState = new Mock<IFormViewState>();
            commandFactory = new Mock<ITopGenCommandFactory>();
            decorator = new Mock<IDecorator>();
            dialogs = new Mock<IDialogHandler>();

            form.Setup(foo => foo.ViewState).Returns(viewState.Object);
            form.Setup(foo => foo.Commands).Returns(commandFactory.Object);
            form.Setup(foo => foo.Decorator).Returns(decorator.Object);
            form.Setup(foo => foo.Dialogs).Returns(dialogs.Object);

            saveAction = new Mock<IAction>();

            viewActions = new Mock<IViewActions>();
            viewActions.Setup(x => x.GetSaveAction(form.Object.Data, Connector.Object)).Returns(saveAction.Object);

            view = new View();
            view.Initialize(form.Object);
            view.Connector = Connector.Object;
            view.Actions = viewActions.Object;
            view.Initialize(form.Object);
        }

        protected override DataSet InitializeEmptyDataSet()
        {
            DataSetBuilder builder = new DataSetBuilder();
            DataSet data = new DataSet();
            //builder.AppendDataSetStructure(data, new TableDefinition());
            return data;
        }

        protected void RaiseActionEvent(Mock<ActionEventArgs> actionEventArgs)
        {
            form.Raise(foo => foo.OnCallingAction += null, new object[] { null, actionEventArgs.Object });
        }

        protected Mock<ActionEventArgs> PrepareActionMessage(string actionCommand)
        {
            Mock<ActionEventArgs> actionEventArgs = new Mock<ActionEventArgs>();
            actionEventArgs.Setup(bar => bar.Action).Returns(actionCommand);
            return actionEventArgs;
        }
        
        protected void RaiseModalDialogActionEvent(Mock<ModalDialogActionEventArgs> modalDialogActionEventArgs)
        {
            form.Raise(foo => foo.OnModalDialogAction += null, new object[] { null, modalDialogActionEventArgs.Object });
        }
        
        protected Mock<ModalDialogActionEventArgs> PrepareModalDialogActionMessage(string dialogId, UIDialogResult result)
        {
            Mock<ModalDialogActionEventArgs> modalDialogActionEventArgs = new Mock<ModalDialogActionEventArgs>();
            Mock<IDialog> dialogMock = new Mock<IDialog>();
            modalDialogActionEventArgs.Setup(bar => bar.Dialog).Returns(dialogMock.Object);
            dialogMock.Setup(x => x.Id).Returns(dialogId);
            dialogMock.Setup(x => x.Result).Returns(result);            
            return modalDialogActionEventArgs;
        }

        protected void RaiseValidatedFieldEvent(Mock<ValidateFieldEventArgs> validateFieldEventArgs)
        {
            form.Raise(foo => foo.OnValidatedField += null, new object[] { null, validateFieldEventArgs.Object });
        }

        protected Mock<ValidateFieldEventArgs> PrepareValidatedFieldEvent(string tableName, string fieldName, DataRow row)
        {
            Mock<ValidateFieldEventArgs> validatedFieldEventArgs = new Mock<ValidateFieldEventArgs>();
            validatedFieldEventArgs.Setup(x => x.TableName).Returns(tableName);
            validatedFieldEventArgs.Setup(x => x.FieldName).Returns(fieldName);
            validatedFieldEventArgs.Setup(x => x.Row).Returns(row);
            return validatedFieldEventArgs;
        }

        protected void RaiseOnInitializedEvent()
        {
            Mock<InitializeEventArgs> eventArgs = new Mock<InitializeEventArgs>();
            form.Raise(foo => foo.OnInitialized += null, new object[] { null, eventArgs.Object });
        }
        
        protected void RaiseOnLoadedEvent()
        {
            Mock<LoadEventArgs> eventArgs = new Mock<LoadEventArgs>();
            form.Raise(foo => foo.OnLoaded += null, new object[] { null, eventArgs.Object });
        }

        protected void RaiseOnClearedEvent()
        {
            Mock<ClearEventArgs> eventArgs = new Mock<ClearEventArgs>();
            form.Raise(foo => foo.OnCleared += null, new object[] { null, eventArgs.Object });
        }
    }
}
