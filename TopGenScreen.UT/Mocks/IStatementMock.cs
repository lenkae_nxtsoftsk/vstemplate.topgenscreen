﻿using Agresso.Interface.CommonExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TopGenScreen.UT.Mocks
{
    public class IStatementMock : IStatement
    {
        string sqlCommand = string.Empty;
        IDictionary<string, object> sqlParameters = new Dictionary<string, object>();

        public IStatementMock() { }

        public IStatementMock(string sql, IDictionary<string, object> parameters)
        {
            sqlCommand = sql;

            foreach(string paramName in parameters.Keys)
            {
                SetParameter(paramName, parameters[paramName]);
            }
        }

        public bool ContainsParameters(IDictionary<string, object> parameters)
        {
            if (parameters == null || sqlParameters == null)
            {
                return false;
            }

            if (parameters.Count != sqlParameters.Count)
            {
                return false;
            }

            foreach (string key in parameters.Keys)
            {
                if (!sqlParameters.ContainsKey(key))
                {
                    return false;
                }

                if (parameters[key] != sqlParameters[key])
                {
                    return false;
                }
            }

            return true;
        }

        public object this[string parameter]
        {
            get
            {
                return sqlParameters[parameter];
            }
            set
            {
                sqlParameters[parameter] = value;
            }
        }

        public string CommandText { get { return sqlCommand; } set { sqlCommand = value; } }

        public IEnumerable ParameterNames
        {
            get
            {
                return sqlParameters.Keys.AsEnumerable();
            }
        }

        public bool UseAgrParser { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public bool Parse { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public void Append(string sql)
        {
            sqlCommand += sql;
        }

        public void Assign(string sql)
        {
            Clear();
            sqlCommand = sql;
        }

        public void Clear()
        {
            sqlCommand = String.Empty;
            sqlParameters.Clear();
        }

        public object GetParameter(string parameter)
        {
            return sqlParameters[parameter];
        }

        public string GetSqlString()
        {
            return sqlCommand;
        }

        public bool IsEmpty()
        {
            return string.IsNullOrEmpty(sqlCommand);
        }

        public void SetParameter(string parameter, object value)
        {
            sqlParameters[parameter] = value;
        }
    }
}
