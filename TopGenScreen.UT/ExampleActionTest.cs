﻿using ACE.Agresso.Utils.Action;
using Moq;
using NUnit.Framework;
using TopGenScreen.Actions;

namespace TopGenScreen.UT
{
    [TestFixture]
	public class ExampleActionTest : ViewTestBase
	{
        [SetUp]
        public new void SetUp()
        {
            base.SetUp();

            saveAction = new Mock<IAction>();
		}

		[Test]
		public void Call_save_action()
		{
			ExecuteAction();
			saveAction.Verify(x => x.Execute(), Times.Once);
		}

		private void ExecuteAction()
		{
			ExampleAction action = new ExampleAction(form.Object, Connector.Object, data, saveAction.Object);
			action.Execute();
		}
	}
}
